gatling-maven-plugin-demo
=========================

Simple showcase of a maven project using the gatling-maven-plugin.

To test it out, simply execute the following command:

    $mvn gatling:test -Dgatling.simulationClass=computerdatabase.BasicSimulation

or simply:

    $mvn gatling:test

note : 

    integration possible avec Jenkins comme avec GitLab-ci
        bat(/"${mvnCMD}" -Dmaven.test.failure.ignore -Dgatling.useOldJenkinsJUnitSupport = true gatling:test/)
		junit '**/target/surefire-reports/TEST-*.xml'

        groovy Jenkinsfile